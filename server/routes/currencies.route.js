const Route = require("express");
const router = new Route();
const CurrenciesController = require("../controllers/currencies.controller");

router.post("/get-currency", CurrenciesController.getCurrency);

module.exports = router;
