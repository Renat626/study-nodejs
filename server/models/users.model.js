const sequelize = require("../db");
const {DataTypes} = require("sequelize");
const Comments = require("./comments.model");

const Users = sequelize.define("users", {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    user_name: {type: DataTypes.STRING, allowNull: false},
    user_surname: {type: DataTypes.STRING, allowNull: false},
    user_email: {type: DataTypes.STRING, allowNull: false, unique: true},
    user_password: {type: DataTypes.STRING, allowNull: false},
    refresh_token: {type: DataTypes.STRING, allowNull: true}
})

Users.hasMany(Comments,{as: 'comments', foreignKey: 'user_id'});

module.exports = Users;