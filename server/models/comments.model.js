const sequelize = require("../db");
const {DataTypes} = require("sequelize");

const Comments = sequelize.define("comments", {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    comment: {type: DataTypes.STRING, allowNull: false}
})

module.exports = Comments;