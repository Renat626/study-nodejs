require("dotenv").config();
const express = require("express");
const process = require('process');
const body_parser = require("body-parser");
const sequelize = require("./db");
const Users = require("./models/users.model");
const router = require("./routes/index");
const cors = require("cors");

const app = express(); 
const PORT = process.env.PORT || 5000;
const corsOpt = {
    origin: "*"
}

app.use(cors(corsOpt));
app.use(body_parser());
app.use("/api", router);

const start = async () => {
    try {
        await sequelize.authenticate();
        await sequelize.sync();
        app.listen(PORT, () => {
            console.log(`Port: ${PORT}`);
        })
    } catch (error) {
        console.log(error);
    }
}

start();