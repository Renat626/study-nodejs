const axios = require("axios");
const xml = require("xml-parse");

class CurrenciesService {
    async getCurrency(date1, date2, currency) {
        const returnArray = [];

        console.log(date1);

        try {
            const answer = await axios.get(`http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=${date1}&date_req2=${date2}&VAL_NM_RQ=R01235`);
        
            let parseDate = xml.parse(answer.data);

            console.log(answer);

            // console.log(parseDate[1].childNodes[0].childNodes);
            let re = /,/gi;

            for (let i = 0; i < parseDate[1].childNodes.length; i++) {
                console.log(parseDate[1].childNodes[i]);
                returnArray.push({
                    date: parseDate[1].childNodes[i].attributes.Date,
                    value: parseDate[1].childNodes[i].childNodes[1].innerXML.replace(re, ".")
                })
                console.log("-------");
            }

            return returnArray;
        } catch (error) {
            console.log(error);

            return returnArray;
        }
        
    }  
}

module.exports = new CurrenciesService();