const CurrenciesService = require("../services/currencies.service");

class CurrenciesController {
    async getCurrency(req, res) {
        const date1 = req.body.date1;
        const date2 = req.body.date2;
        const currency = req.body.currency;

        const answer = await CurrenciesService.getCurrency(date1, date2, currency);

        res.json(answer);
    }
}

module.exports = new CurrenciesController();