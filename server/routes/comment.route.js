const Router = require("express");
const router = new Router();
const CommentController = require("../controllers/comment.controller");

router.get("/get-all", CommentController.getAll);
router.get("/get", CommentController.getCommentsOfUser);
router.post("/create", CommentController.create);

module.exports = router;