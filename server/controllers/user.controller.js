const Users = require("../models/users.model");
const userService = require("../services/user.service");

class userController {
    async registration(req, res) {
        const name = req.body.name;
        const surname = req.body.surname;
        const email = req.body.email;
        const password = req.body.password;

        const answer = await userService.registration(name, surname, email, password);

        if (answer.status == false) {
            res.status(400).json(answer);
        } else {
            res.json(answer);
        }
    }

    async login(req, res) {
        const email = req.body.email;
        const password = req.body.password;

        const answer = await userService.login(email, password);

        if (answer.status == false) {
            res.status(400).json(answer);
        } else {
            res.json(answer);
        }
    }

    async check(req, res) {
        if (req.headers.authorization == undefined) {
            return res.status(401).json({
                "status": false,
                "statusText": "Токен не действителен"
            });
        }

        const arr =  req.headers.authorization.split(' ');
        const access_token = arr[1];

        const answer = await userService.check(access_token);

        res.json(answer);
    }

    async logout(req, res) {
        // if (req.headers.authorization == undefined) {
        //     return res.json({
        //         "status": false,
        //         "statusText": "Токен не действителен"
        //     });
        // }

        const arr =  req.headers.authorization.split(' ');
        const access_token = arr[1];

        const answer = await userService.logout(access_token);

        res.json(answer);
    }

    async getAll(req, res) {
        const answer = await userService.getAll();

        res.json(answer);
    }
}

module.exports = new userController();