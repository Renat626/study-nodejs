const TokenService = require("./token.service");
const User = require("../models/users.model");
const bcrypt = require("bcrypt");

class userService {
    async registration(name, surname, email, password) {
        const return_obj = {
            status: true,
            status_text: "",
            access_token: ""
        }
        
        let checkUser = "";

        try {
            checkUser = await User.findOne({where: {user_email: email}});
        } catch (error) {
            console.log(error);

            return_obj.status = false;
            return_obj.status_text = "Что-то пошло не так.";
            return_obj.access_token = "";
            return return_obj;
        }

        if (checkUser) {
            console.log("exists");
            return_obj.status = false;
            return_obj.status_text = "Такой пользователь уже существует либо что-то пошло не так.";

            return return_obj;
        } else {
            console.log("doesn't exist");
            try {
                const password_hash = await bcrypt.hash(password, 5);
                const user = await User.create({user_name: name, user_surname: surname, user_email: email, user_password: password_hash});

                console.log(user);

                const tokens = await TokenService.generateTokens({user_id: user.dataValues.id, user_name: user.dataValues.user_name, user_surname: user.dataValues.user_surname, user_email: user.dataValues.user_email}, "registration");

                console.log(tokens);

                await User.update({refresh_token: tokens.refreshToken},{where: {user_email: user.dataValues.user_email}})

                return_obj.status = true;
                return_obj.status_text = "Пользователь успешно создан.";
                return_obj.access_token = tokens.accessToken;

                return return_obj;
            } catch (error) {
                console.log(error);

                return_obj.status = false;
                return_obj.status_text = "Такой пользователь уже существует либо что-то пошло не так.";

                return return_obj;
            }
        }
    }

    async login(email, password) {
        const return_obj = {
            status: true,
            status_text: "",
            access_token: ""
        }

        let checkUser = "";

        try {
            checkUser = await User.findOne({where: {user_email: email}});
        } catch (error) {
            console.log(error);

            return_obj.status = false;
            return_obj.status_text = "Что-то пошло не так.";
            return_obj.access_token = "";
            return return_obj;
        }

        if (checkUser) {
            try {
                const compare = await bcrypt.compare(password, checkUser.user_password);

                if (!compare) {
                    return_obj.status = false;
                    return_obj.status_text = "Не верный пароль.";
                    return_obj.access_token = "";
                    return return_obj;
                }

                const tokens = await TokenService.generateTokens({user_id: checkUser.dataValues.id, user_name: checkUser.dataValues.user_name, user_surname: checkUser.dataValues.user_surname, user_email: checkUser.dataValues.user_email});

                await User.update({refreshToken: tokens.accessToken}, {where: {id: checkUser.dataValues.id}});

                return_obj.status = true;
                return_obj.status_text = "Успешная авторизация.";
                return_obj.access_token = tokens.accessToken;
                return return_obj;
            } catch (error) {
                console.log(error);

                return_obj.status = false;
                return_obj.status_text = "Что-то пошло не так.";
                return_obj.access_token = "";
                return return_obj;
            }
        } else {
            return_obj.status = false;
            return_obj.status_text = "Пароль или логин не верные.";
            return_obj.access_token = "";
            return return_obj;
        }
    }

    async check(token) {
        const status = await TokenService.checkAccessToken(token);
        return status;
    }

    async logout(token) {
        const return_obj = {
            status: true,
            status_text: ""
        }

        const userData = await TokenService.parseAccessToken(token);

        await User.update({refreshToken: ""}, {where: {id: userData.user_id}});

        return_obj.status = true;
        return_obj.status_text = "Пользователь вышел.";
        return return_obj;
    }

    async getAll() {
        const users = await User.findAll({attributes: ["id", "user_name", "user_surname", "user_email"]});

        return users;
    }
}

module.exports = new userService();