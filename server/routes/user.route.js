const Router = require("express");
const router = new Router();
const userController = require("../controllers/user.controller");

router.post("/registration", userController.registration);
router.post("/login", userController.login);
router.get("/check", userController.check);
router.get("/logout", userController.logout);
router.get("/get-all", userController.getAll);

module.exports = router;