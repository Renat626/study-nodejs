const Router = require("express");
const router = new Router();
const user_route = require("./user.route");
const comment_route = require("./comment.route");
const currencies_route = require("./currencies.route");

router.use("/user", user_route);
router.use("/comment", comment_route);
router.use("/currencies", currencies_route);

module.exports = router;