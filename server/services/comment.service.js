const Comments = require("../models/comments.model");

class CommentService {
    async getAll() {

    }

    async getCommentsOfUser() {

    }

    async create(text, user_id) {
        await Comments.create({comment: text, user_id: user_id});

        return true;
    }
}

module.exports = new CommentService();