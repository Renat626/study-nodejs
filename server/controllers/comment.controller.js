const CommentService = require("../services/comment.service");
const TokenService = require("../services/token.service")

class CommentController {
    async getAll(req, res) {

    }

    async getCommentsOfUser(req, res) {

    }

    async create(req, res) {
        const return_obj = {
            status: true,
            status_text: ""
        }
        const arr =  req.headers.authorization.split(' ');
        const access_token = arr[1];
        const comment = req.body.text;

        const checkUser = await TokenService.checkAccessToken(access_token);
        
        if (checkUser) {
            const userData = await TokenService.parseAccessToken(access_token);
            const answer = await CommentService.create(comment, userData.user_id);
            
            return_obj.status_text = "Комментарий добавлен.";
            return res.json(return_obj);
        } else {
            return_obj.status = false;
            return_obj.status_text = "Токен не действителен.";
            return res.status(400).json(return_obj);
        }
    }
}

module.exports = new CommentController();